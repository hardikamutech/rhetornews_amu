<?php

defined('_ENGINE') or die('Access Denied');
return array(
    'adapter' => 'mysqli',
    'params' =>
    array(
        'host' => 'localhost',
//        'username' => 'amuteiun_rhetorn',
//        'password' => '9Bl2iTnpL%Hf',
//        'dbname' => 'amuteiun_rhetornews',
        'username' => 'root',
        'password' => '',
        'dbname' => 'rhetornews_amu',
        'charset' => 'UTF8',
        'adapterNamespace' => 'Zend_Db_Adapter',
    ),
    'isDefaultTableAdapter' => true,
    'tablePrefix' => 'engine4_',
    'tableAdapterClass' => 'Engine_Db_Table',
);
?>