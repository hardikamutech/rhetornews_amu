<?php
/**
* SocialEngine
*
* @category   Application_Widget
* @package    Rss
* @copyright  Copyright 2006-2010 Webligo Developments
* @license    http://www.socialengine.com/license/
* @version    $Id: index.tpl 9747 2012-07-26 02:08:08Z john $
* @author     John
*/
?>

<script type="text/jscript" src="<?php echo $this->layout()->staticBaseUrl ?>application/themes/modern/js/jquery.min.js"></script>

<script type="text/javascript">
    en4.core.runonce.add(function() {
        $$('.rss_desc').enableLinks();
    });
</script>



<link rel="stylesheet" href="<?php echo $this->layout()->staticBaseUrl ?>application/themes/modern/css/flexslider.css" type="text/css" media="screen" />
<script defer src="<?php echo $this->layout()->staticBaseUrl ?>application/themes/modern/js/jquery.flexslider.js"></script>
<script type="text/javascript">

    $(window).load(function() {
        $('.flexslider').flexslider({
            animation: "slide",
            animationLoop: false,
            itemWidth: 251,
            itemMargin: 45,
            pausePlay: false,
            start: function(slider) {
                $('body').removeClass('loading');
            }
        });
    });
</script>
<script type="text/javascript" src="<?php echo $this->layout()->staticBaseUrl ?>application/themes/modern/js/shCore.js"></script>
<script type="text/javascript" src="<?php echo $this->layout()->staticBaseUrl ?>application/themes/modern/js/shBrushXml.js"></script>
<script type="text/javascript" src="<?php echo $this->layout()->staticBaseUrl ?>application/themes/modern/js/shBrushJScript.js"></script>



<?php if( !empty($this->channel) ): ?>

<ul id="">
    <div class="slider_area">

        <div id="container" class="cf">

            <div id="main" role="main">
                <div class="latest_news">
                    <span class="heading_latest">Latest News</span>
                    <span class="top_image"></span>
                    <span class="bottom_image"></span>

                </div>

                <section class="slider">
                    <div class="flexslider carousel">
                        <ul class="slides">
                            <?php $count=0;foreach( $this->channel['items'] as $item ): $count++ ?>


                            <li class="new_margin">
                                <img src="<?php echo $item['image']['uri'] ?>" alt="<?php echo $item['image']['title'] ?>" title="<?php echo $item['image']['title'] ?>" >
                                <div>
                                    <p><?php echo $this->htmlLink($item['link'], substr($item['title'],0,80),
                                        array('target' => '_blank', 'class' => 'rss_link_' . $count .' slide_title')) ?></p>
                                </div>
                            </li>

                            <?php endforeach; ?>
                        </ul>
                    </div>
                </section>
            </div>
        </div>
    </div>


</ul>

<?php endif; ?>

<?php if( false ): ?>
<br />
<span class="rss_fetched_timestamp">
    <?php if( $this->isCached ): ?>
    <?php echo $this->translate('Results last fetched at %1$s',
    $this->locale()->toDateTime($this->channel['fetched'])) ?>
    <?php else: ?>
    <?php echo $this->translate('Results are current') ?>
    <?php endif ?>
</span>
<?php endif ?>

