<?php
/**
* SocialEngine
*
* @category   Application_Widget
* @package    Rss
* @copyright  Copyright 2006-2010 Webligo Developments
* @license    http://www.socialengine.com/license/
* @version    $Id: index.tpl 9747 2012-07-26 02:08:08Z john $
* @author     John
*/
?>

<script type="text/jscript" src="<?php echo $this->layout()->staticBaseUrl ?>application/themes/modern/js/jquery.min.js"></script>
<script type="text/jscript" src="<?php echo $this->layout()->staticBaseUrl ?>application/themes/modern/js/pin.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $('#pinBoot').pinterest_grid({
            no_columns: 3,
            padding_x: 50,
            padding_y: 25,
            margin_bottom: 50,
            single_column_breakpoint: 700
        });
    });
    en4.core.runonce.add(function() {
        $$('.rss_desc').enableLinks();
    });
</script>


<?php if( !empty($this->channel) ): ?>

<ul id="pinBoot">
    <?php $count=0;foreach( $this->channel['items'] as $item ): $count++ ?>
    <li class="rss_item white-panel">
        <div class="rss_item_<?php echo $count ?>">

            <div class="feed_img_div">
                <img src="<?php echo $item['image']['uri'] ?>" alt="<?php echo $item['image']['title'] ?>" title="<?php echo $item['image']['title'] ?>" >
            </div>
            <br>
            <?php echo $this->htmlLink($item['link'], $item['title'],
            array('target' => '_blank', 'class' => 'rss_link_' . $count .' feed_title')) ?>
            <p class="rss_desc">
                <?php if( $this->strip ): ?>
                <?php //echo $this->string()->truncate($this->string()->stripTags($item['description']), 350) ?>
                <?php else: ?>
                <?php //echo $item['description'] ?>
                <?php endif ?>
            </p>
        </div>
        <div class="rss_time">


            <div class="bottom_part">
                <div class="bottom_left">
                    <span class="font_icon">
                        <i class="fa fa-share"></i>
                    </span>
                    <span class="font_icon">
                        <i class="fa fa-thumbs-up"></i>
                    </span>
                    <span class="date">
                        <?php echo $this->locale()->toDate(strtotime($item['pubDate']), array('size' => '')) ?>
                    </span>
                </div>

                <div class="bottom_right">
                    <span class="read_more">
                        <?php echo $this->htmlLink($item['link'], 'Read More',
                        array('target' => '_blank', 'class' => 'rss_link_' . $count)) ?>
                    </span>
                </div>
            </div>


        </div>

    </li>
    <?php endforeach; ?>

</ul>

<?php endif; ?>

<?php if( false ): ?>
<br />
<span class="rss_fetched_timestamp">
    <?php if( $this->isCached ): ?>
    <?php echo $this->translate('Results last fetched at %1$s',
    $this->locale()->toDateTime($this->channel['fetched'])) ?>
    <?php else: ?>
    <?php echo $this->translate('Results are current') ?>
    <?php endif ?>
</span>
<?php endif ?>

