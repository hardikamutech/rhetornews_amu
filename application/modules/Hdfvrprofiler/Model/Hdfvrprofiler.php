<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Hdfvrprofiler
 * @copyright  Copyright 2010 HDFVR.com
 * @license    http://avchat.net/license
 * @version
 * @author     Mihai Frentiu <mihai@avchathq.com>
 */

/**
 * @category   Application_Extensions
 * @package    Hdfvrprofiler
 * @copyright  Copyright 2010 HDFVR.com
 * @license    http://avchat.net/license
 */
class Hdfvrprofiler_Model_Hdfvrprofiler extends Core_Model_Item_Abstract
{
	protected $_parent_type = 'user';

	protected $_owner_type = 'user';

	protected $_parent_is_owner = true;
	
	/**
	 * The action likes
	 *
	 * @var mixed
	 */
	protected $_likes;

	// Interfaces

	/**
	 * Gets a proxy object for the comment handler
	 *
	 * @return Engine_ProxyObject
	 **/
	public function comments()
	{
		return new Engine_ProxyObject($this, Engine_Api::_()->getDbtable('comments', 'core'));
	}

	/**
	 * Gets a proxy object for the like handler
	 *
	 * @return Engine_ProxyObject
	 **/
	public function likes()
	{
		return new Engine_ProxyObject($this, Engine_Api::_()->getDbtable('likes', 'core'));
	}

	/**
	 * Gets a proxy object for the tags handler
	 *
	 * @return Engine_ProxyObject
	 **/
	public function tags()
	{
		return new Engine_ProxyObject($this, Engine_Api::_()->getDbtable('tags', 'core'));
	}
	public function getRichContent($view = false, $params = array())
	{
		
		//var_dump($this->type);
		//return "";
		$session = new Zend_Session_Namespace('mobile');
		$mobile = $session->mobile;

		// if video type is uploaded
		//var_dump("555");
		//die();
		$video_duration = "";
		if( $this->duration ) {
			if( $this->duration >= 3600 ) {
				$duration = gmdate("H:i:s", $this->duration);
			} else {
				$duration = gmdate("i:s", $this->duration);
			}
			$duration = ltrim($duration, '0:');
		
			$video_duration = "<span class='video_length'>".$duration."</span>";
		}
		$thumb = Zend_Registry::get('Zend_View')->itemPhoto($this, 'thumb.video.activity');
		
		if( $this->photo_id ) {
			$thumb = Zend_Registry::get('Zend_View')->itemPhoto($this, 'thumb.video.activity');
		} else {
			$thumb = '<img alt="" src="' . Zend_Registry::get('StaticBaseUrl') . 'application/modules/Hdfvrprofiler/externals/images/video.png">';
		}
		//var_dump($video_duration);
		//return "";
		$viewer_id = Engine_Api::_()->user()->getViewer()->getIdentity();
		//$subject = Engine_Api::_()->core()->
		//var_dump($viewer_id);
		//var_dump($this->id_user);
		//return "";
		if($viewer_id == $this->id_user){
			$thumb = '<a id="video_thumb_'.$this->hdfvrprofiler_id.'" class="video_thumb smoothbox" href="hdfvrprofiler/index/view/hdfvrprofiler_id/'.$this->hdfvrprofiler_id.'"> <div class="video_thumb_wrapper">'.$video_duration.$thumb.'</div>
			</a>';
		}
		else{
			$thumb = '<a id="video_thumb_'.$this->hdfvrprofiler_id.'" class="video_thumb smoothbox" href="hdfvrprofiler/index/view_friend/user_id/'.$this->id_user.'_'.$this->hdfvrprofiler_id.'"> <div class="video_thumb_wrapper">'.$video_duration.$thumb.'</div>
                  </a>';
		}
		//var_dump($thumb);
		return $thumb;
		
		
		//



		//$video_location = Engine_Api::_()->storage()->get(0, "2")->getHref();

		//var_dump($video_location);
		//return "";

		$videoEmbedded = $this->compileFlowPlayer($video_location, $view);

		

		// $view == false means that this rich content is requested from the activity feed
		if($view==false){

			// prepare the duration
			//
			$video_duration = "";
			if( $this->duration ) {
				if( $this->duration >= 3600 ) {
					$duration = gmdate("H:i:s", $this->duration);
				} else {
					$duration = gmdate("i:s", $this->duration);
				}
				$duration = ltrim($duration, '0:');

				$video_duration = "<span class='video_length'>".$duration."</span>";
			}

			// prepare the thumbnail
			$thumb = Zend_Registry::get('Zend_View')->itemPhoto($this, 'thumb.video.activity');

			if( $this->photo_id ) {
				$thumb = Zend_Registry::get('Zend_View')->itemPhoto($this, 'thumb.video.activity');
			} else {
				$thumb = '<img alt="" src="' . Zend_Registry::get('StaticBaseUrl') . 'application/modules/Hdfvrprofiler/externals/images/video.png">';
			}

			if( !$mobile ){
				$thumb = '<a id="video_thumb_'.$this->video_id.'" style="" href="javascript:void(0);" onclick="javascript:var myElement = $(this);myElement.style.display=\'none\';var next = myElement.getNext(); next.style.display=\'block\';">
				<div class="video_thumb_wrapper">'.$video_duration.$thumb.'</div>
				</a>';
			} else {
				$thumb = '<a id="video_thumb_'.$this->video_id.'" class="video_thumb" href="javascript:void(0);" onclick="javascript: $(\'videoFrame'.$this->video_id.'\').style.display=\'block\'; $(\'videoFrame'.$this->video_id.'\').src = $(\'videoFrame'.$this->video_id.'\').src; var myElement = $(this); myElement.style.display=\'none\'; var next = myElement.getNext(); next.style.display=\'block\';">
				<div class="video_thumb_wrapper">'.$video_duration.$thumb.'</div>
				</a>';
			}

			// prepare title and description
			$title = "<a href='".$this->getHref($params)."'>$this->title</a>";
			$tmpBody = strip_tags($this->description);
			$description = "<div class='video_desc'>".(Engine_String::strlen($tmpBody) > 255 ? Engine_String::substr($tmpBody, 0, 255) . '...' : $tmpBody)."</div>";

			$videoEmbedded = $thumb.'<div id="video_object_'.$this->video_id.'" class="video_object">'.$videoEmbedded.'</div><div class="video_info">'.$title.$description.'</div>';

		}

		return $videoEmbedded;
	}

	
	

	
}