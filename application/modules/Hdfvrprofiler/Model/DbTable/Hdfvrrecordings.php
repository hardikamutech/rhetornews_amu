<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Hdfvrprofiler
 * @copyright  Copyright 2010 HDFVR.com
 * @license    http://avchat.net/license
 * @version    
 * @author     Mihai Frentiu <mihai@avchathq.com>
 */

/**
 * @category   Application_Extensions
 * @package    Hdfvrprofiler
 * @copyright  Copyright 2010 HDFVR.com
 * @license    http://avchat.net/license
 */
class Hdfvrprofiler_Model_DbTable_Hdfvrrecordings extends Engine_Db_Table
{
  protected $_name = 'hdfvr_recordings';

}