<?php
###################### Video Player Configuration File ############
######################## MANDATORY FIELDS #########################


//-----------------------------------
//Get SETTINGS
//-----------------------------------
if(isset($_COOKIE['hdfvrprofilerplayer_settings']) && $_COOKIE['hdfvrprofilerplayer_settings'] != ''){
	$primitive_settings = explode('&', $_COOKIE['hdfvrprofilerplayer_settings']);
	$settings = array();
	foreach ($primitive_settings as $p_setting){
		list($key, $value) = explode('=', $p_setting);
		$settings[$key] = $value;
	}
}


//connectionstring:String
//affects: recorder, player
//description: the rtmp connection string to the avchat2 application on your Flash Media Server server
//values: 'rtmp://localhost/videorecorder/_definst_', 'rtmp://myfmsserver.com/videorecorder/_definst_', etc...

if(isset($settings['rtmp_connectionstring']) && $settings['rtmp_connectionstring'] != ''){
	$config['connectionstring'] = $settings['rtmp_connectionstring'];
}else{
	$config['connectionstring']='rtmp://localhost/hdfvr/_definst_';
}

//incomingBuffer: Number
//affects: player
//desc: The size of the buffer for incoming data from the server, in seconds, does not affect the recording process, only the playback process (in both the recorder and player). Before data is played it is buffered in this buffer.
//values: 1,2,etc...
if(isset($settings['playback_buffer']) && $settings['playback_buffer'] != ''){
	$config['incomingBuffer']=$settings['playback_buffer'];
}else{
	$config['incomingBuffer']=2;
}

//streamName: String
//affects: player
//desc: the name fo the .flv file on the FMS/Red5 server for the player to PLAY, including the .flv extension. This does not affect the recorder as the recorder always generates a new file name!
//values:
if(isset($settings['filename']) && $settings['filename'] != ''){
$config['streamName']=$settings['filename'].'.flv';
}else{
$config['streamName']='';	
}

//autoPlay: String
//affects: player
//desc: weather the audio stream should play automatically or  wait for the user to press the PLAY button
//values: false, true
$config['autoPlay']='true';

##################### DO NOT EDIT BELOW ############################
foreach ($config as $key => $value){
	echo '&'.$key.'='.$value;
}
?>