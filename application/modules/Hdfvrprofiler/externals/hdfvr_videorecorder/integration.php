<?php
//-----------------------------------
//GET Permissions
//-----------------------------------
if(isset($_COOKIE['hdfvrprofiler_permissions']) && $_COOKIE['hdfvrprofiler_permissions'] != ''){
	$primitive_permissions = explode('&', $_COOKIE['hdfvrprofiler_permissions']);
	$settings = array();
	foreach ($primitive_permissions as $p_permission){
		list($key, $value) = explode('=', $p_permission);
		$permissions[$key] = $value;
	}
}

$config['hideSaveButton']=1;

if(isset($permissions['hdfvr_allow_rec']) && $permissions['hdfvr_allow_rec']){
	
	//-----------------------------------
	//Get SETTINGS
	//-----------------------------------
	if(isset($_COOKIE['hdfvrprofiler_settings']) && $_COOKIE['hdfvrprofiler_settings'] != ''){
		$primitive_settings = explode('&', $_COOKIE['hdfvrprofiler_settings']);
		$settings = array();
		foreach ($primitive_settings as $p_setting){
			list($key, $value) = explode('=', $p_setting);
			$settings[$key] = $value;
		}
		
		
		//Setup connectionstring
		if(isset($settings['rtmp_connectionstring']) && $settings['rtmp_connectionstring'] != ''){
			$config['connectionstring'] = $settings['rtmp_connectionstring'];
		}
		
		//Setup buffer
		if(isset($settings['buffer_size']) && $settings['buffer_size'] != ''){
			$config['outgoingBuffer']=$settings['buffer_size'];
		}
		
		//Setup user id
		if(isset($settings['buffer_size']) && $settings['buffer_size'] != ''){
			$config['userId']= $settings['userid'];
		}
		
		//Setup audio/video quality profile url
		$config['qualityurl']='audio_video_quality_profiles/wide/1280x720x30x90.xml';
		if(isset($settings['quality_profile']) && $settings['quality_profile'] != ''){
			list($type, $profile) = explode('_',$settings['quality_profile']);
			
			if($type == 'wide'){
				$config['qualityurl']='audio_video_quality_profiles/wide/'.$profile.'.xml';
			}else{
				$config['qualityurl']='audio_video_quality_profiles/'.$profile.'.xml';
			}
		}
	}
}else{
	$config['maxRecordingTime']=0;
}

?>