<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Hdfvrprofiler
 * @copyright  Copyright 2010 HDFVR.com
 * @license    http://avchat.net/license
 * @version    
 * @author     Mihai Frentiu <mihai@avchathq.com>
 */

/**
 * @category   Application_Extensions
 * @package    Hdfvrprofiler
 * @copyright  Copyright 2010 HDFVR.com
 * @license    http://avchat.net/license
 */
class Hdfvrprofiler_AdminLevelController extends Core_Controller_Action_Admin
{
  public function editAction()
  {
  	$this->view->navigation = $navigation = Engine_Api::_()->getApi('menus', 'core') 
  	->getNavigation('hdfvrprofiler_admin_main', array(), 'hdfvrprofiler_admin_main_level');
    
  	// Get level id
  	if( null !== ($id = $this->_getParam('id')) ) {
  		$this->view->level = $level = Engine_Api::_()->getItem('authorization_level', $id);
  	} else {
  		$this->view->level = $level = Engine_Api::_()->getItemTable('authorization_level')->getDefaultLevel();
  		$id = $level->level_id;
  	}
  	
  	
   	//$viewer = Engine_Api::_()->user()->getViewer();
   	// Get level id
   	//$id = $viewer->level_id; 
    
   
    
    $this->view->form = $form = new Hdfvrprofiler_Form_Admin_Level_Edit();
    $permissionsTable = Engine_Api::_()->getDbtable('permissions', 'authorization');
    
    
    // Posting form
    if( $this->getRequest()->isPost() )
    {
      if( $form->isValid($this->getRequest()->getPost()) )
      {
        $values = $form->getValues();
        //$level->title = $values['title'];
        //$level->description = $values['description'];
        $level->save();

        // update user profile editing permissions
        /*
        $permissionsTable->setAllowed('user', $id, 'edit', $values['edit']);
        // destroy user-edit permission from values array so it doesnt get added to the general perm
        unset($values['edit']);
        */
        
        // set level specific settings for profile, activity and html comments
        $permissionsTable->setAllowed('hdfvrprofiler', $level->level_id, $values);
        $form->addNotice('Your changes have been saved.');
     }
    }

    // Initialize data
    else
    {
      $form->populate($level->toArray());
      $form->populate($permissionsTable->getAllowed('hdfvrprofiler', $id, array_keys($form->getValues())));
      //$form->getElement('title')->setValue($level->title);
      }
  }


  public function setdefaultAction(){
  	//$viewer = Engine_Api::_()->user()->getViewer();
  	// Get level id
  	//$id = $viewer->level_id;
    $id = $this->_getParam('level_id', null);
    $table = $this->_helper->api()->getDbtable('levels', 'authorization');
    $db = $table->getAdapter();
    $db->beginTransaction();

    try
    {
      // get current default and de-flag the item
      $select = $table->select()
        ->where('flag = ?', 'default')
        ->limit(1);
      $default_level = $table->fetchRow($select);
      $default_level->flag = "";
      $default_level->save();

      // set the current item to default
      $this->view->level = $level = Engine_Api::_()->getItem('authorization_level', $id);
      $level->flag = 'default';
      $level->save();
      $db->commit();

    }

    catch( Exception $e )
    {
      $db->rollBack();
      throw $e;
    }
  }
}