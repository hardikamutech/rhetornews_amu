<div style="padding:10px 0 10px 10px">
    <p style="text-align:right"><a href="javascript:void(0);" onclick="parent.Smoothbox.close();">x</a></p>
    <h3><?php echo $this->title; ?></h3>

    <?php if($this->loggedIn){ ?>
    <script type="text/javascript">
        function btRecordPressed() {
            //alert("btRecordPressed");
            //this function is called whenever the Record button is pressed to start a recording
        }

        function btStopRecordingPressed() {
            //alert("btStopRecordingPressed");
            //this function is called whenever a recording is stopped
        }

        function btPlayPressed() {
            //alert("btPlayPressed");
            //this function is called whenever the Play button is pressed to start/resume playback
        }

        function btPausePressed() {
            //alert("btPausePressed");
            //this function is called whenever the Pause button is pressed during playback
        }

        function onUploadDone(streamName, streamDuration, userId) {
            //alert("onUploadDone("+streamName+","+streamDuration+","+userId+")");


            var stream_name_container = document.getElementById('stream_name');
            var stream_duration_container = document.getElementById('stream_duration');
            var submit_btn = document.getElementById('submit_with_post');
            var submit_btn_without_post = document.getElementById('submit_without_post');



            stream_name_container.value = streamName;
            stream_duration_container.value = Math.round(streamDuration);
            submit_btn.style.display = "block";
            submit_btn_without_post.style.display = "block";



            //this function is called when the video/audio stream has been all sent to the video server and has been saved to the video server HHD,
            //on slow client->server connections, because the data can not reach the video server in real time, it is stored in the recorder\'s buffer until it is sent to the server, you can configure the buffer size in avc_settings.XXX

            //this function is called with 3 parameters:
            //streamName: a string representing the name of the stream recorded on the video server including the .flv extension
            //userId: the userId sent via flash vars or via the avc_settings.XXX file, the value in the avc_settings.XXX file takes precedence if its not empty
            //duration of the recorded video/audio file in seconds but acccurate to the millisecond (like this: 4.322)
        }

        function onSaveOk(streamName, streamDuration, userId) {


            //the user pressed the [save] button inside the recorder and the save_video_to_db.XXX script returned save=ok
        }

        function onSaveFailed(streamName, streamDuration, userId) {
            //alert("onSaveFailed("+streamName+","+streamDuration+","+userId+")");

            //the user pressed the [save] button inside the recorder but the save_video_to_db.XXX script returned save=fail
        }

        function onSaveJpgOk(streamName, userId) {
            //alert("onSaveJpgOk("+streamName+","+userId+")");

            //the user pressed the [save] button inside the recorder and the save_video_to_db.XXX script returned save=ok
        }

        function onSaveJpgFailed(streamName, userId) {
            //alert("onSaveJpgFailed("+streamName+","+userId+")");

            //the user pressed the [save] button inside the recorder but the save_video_to_db.XXX script returned save=fail
        }

        function clickSaveCustom() {
            if (submitParam == 0) {
                var fmo = getFlashMovieObject('videorecorder');
                //if (!e) var e = window.event;

                fmo.clickSave();

                //e.cancelBubble = true;
                //if (e.stopPropagation) e.stopPropagation();
                return false;
            } else {
                //alert('ico');
                document.form.submit();
                return true;
            }

        }
    </script>


    <div style="text-align:center">
        <?php echo $this->recorder; ?>
    </div>

    <div>
        <h3>Usage instructions:</h3>
        <p>1. Click the RECORD button.</p>
        <p>2. Click the STOP button after you finished the recording.</p>
        <p>3. If you want to review the recording, press play.</p>
        <p>4. To add the video to comment, click [Add Video as Comment]</p>
    </div>

    <?php echo $this->form->render($this); ?>
    <?php } ?>
</div>