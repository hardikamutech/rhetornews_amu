<form method="post" class="global_form_popup" action="<?php echo $this->url(array()) ?>">
	<div style="padding:10px 0 10px 10px">
		
		<h3><?php echo $this->translate("Delete Video Profile?") ?></h3>
		<p>
	      <?php echo $this->translate("Are you sure that you want to delete the video profile? It will not be recoverable after being deleted.") ?>
	    </p>
	    <br />
	    <p>
		<button type='submit'><?php echo $this->translate("Delete") ?></button>
	      <?php echo $this->translate("or") ?>
				<a href='javascript:void(0);' onclick='javascript:parent.Smoothbox.close()'>
					<?php echo $this->translate("cancel") ?>
				</a>
	</div>
</form>