INSERT IGNORE INTO `engine4_activity_actiontypes` (`type`, `module`,  `body`,  `enabled`,  `displayable`,  `attachable`,  `commentable`,  `shareable`, `is_generated`) VALUES
('recorded_video_new', 'hdfvrprofiler', '{item:$subject} posted a new profile video:', '1', '5', '1', '3', '1', 0);

INSERT IGNORE INTO `engine4_core_menuitems` (`name`, `module`, `label`, `plugin`, `params`, `menu`, `submenu`, `order`) VALUES
('core_main_hdfvrprofiler_delete', 'hdfvrprofiler', 'Delete My Video Profile', 'Hdfvrprofiler_Plugin_Menus', '', 'user_home', '', 9);

UPDATE `engine4_core_menuitems` SET `plugin` = 'Hdfvrprofiler_Plugin_Menus' WHERE `name` ='core_main_hdfvrprofiler_view';

UPDATE `engine4_core_menuitems` SET `plugin` = 'Hdfvrprofiler_Plugin_Menus' WHERE `name` ='core_main_hdfvrprofiler';

INSERT IGNORE INTO `engine4_authorization_permissions` (`level_id`, `type`, `name`, `value`, `params`) VALUES
('1', 'hdfvrprofiler', 'comment', '1', 'NULL'),
('2', 'hdfvrprofiler', 'comment', '1', 'NULL'),
('3', 'hdfvrprofiler', 'comment', '1', 'NULL'),
('4', 'hdfvrprofiler', 'comment', '1', 'NULL'),
('5', 'hdfvrprofiler', 'comment', '0', 'NULL'),
('1', 'hdfvrprofiler', 'auth_view', '5', '["everyone","owner_network","owner_member_member","owner_member","owner"]'),
('2', 'hdfvrprofiler', 'auth_view', '5', '["everyone","owner_network","owner_member_member","owner_member","owner"]'),
('3', 'hdfvrprofiler', 'auth_view', '5', '["everyone","owner_network","owner_member_member","owner_member","owner"]'),
('4', 'hdfvrprofiler', 'auth_view', '5', '["everyone","owner_network","owner_member_member","owner_member","owner"]'),
('1', 'hdfvrprofiler', 'auth_comment', '5', '["everyone","owner_network","owner_member_member","owner_member","owner"]'),
('2', 'hdfvrprofiler', 'auth_comment', '5', '["everyone","owner_network","owner_member_member","owner_member","owner"]'),
('3', 'hdfvrprofiler', 'auth_comment', '5', '["everyone","owner_network","owner_member_member","owner_member","owner"]'),
('4', 'hdfvrprofiler', 'auth_comment', '5', '["everyone","owner_network","owner_member_member","owner_member","owner"]'),
('1', 'hdfvrprofiler', 'view', '1', 'NULL'),
('2', 'hdfvrprofiler', 'view', '1', 'NULL'),
('3', 'hdfvrprofiler', 'view', '1', 'NULL'),
('4', 'hdfvrprofiler', 'view', '1', 'NULL'),
('5', 'hdfvrprofiler', 'view', '0', 'NULL');


#DROP PROCEDURE IF EXISTS `rename_table_hdfvr_recordings_if_exists`;

CREATE PROCEDURE IF NOT EXISTS `rename_table_hdfvr_recordings_if_exists`(IN dbName tinytext)
BEGIN
IF (SELECT COUNT(*)
    FROM information_schema.tables
    WHERE table_name = 'engine4_hdfvr_recordings'
    ) > 0 THEN 
    		RENAME TABLE `engine4_hdfvr_recordings` TO `engine4_hdfvrprofiler_hdfvrprofilers` ;
END IF;
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS
WHERE 
	TABLE_SCHEMA = dbName AND
	TABLE_NAME = 'engine4_hdfvrprofiler_hdfvrprofilers' AND 
	COLUMN_NAME = 'hdfvrprofiler_id')
THEN
   ALTER TABLE `engine4_hdfvrprofiler_hdfvrprofilers` CHANGE `id` `hdfvrprofiler_id` INT( 10 ) NOT NULL AUTO_INCREMENT;
END IF;
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS
WHERE 
	TABLE_SCHEMA = dbName AND
	TABLE_NAME = 'engine4_hdfvrprofiler_hdfvrprofilers' AND 
	COLUMN_NAME = 'photo_id')
THEN
   ALTER TABLE `engine4_hdfvrprofiler_hdfvrprofilers` ADD COLUMN photo_id INT( 10 ) NOT NULL;
END IF;
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS
WHERE 
	TABLE_SCHEMA = dbName AND
	TABLE_NAME = 'engine4_hdfvrprofiler_hdfvrprofilers' AND 
	COLUMN_NAME = 'owner_id')
THEN
   ALTER TABLE `engine4_hdfvrprofiler_hdfvrprofilers` ADD COLUMN owner_id INT( 11 ) NOT NULL;
END IF;
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS
WHERE 
	TABLE_SCHEMA = dbName AND
	TABLE_NAME = 'engine4_hdfvrprofiler_hdfvrprofilers' AND 
	COLUMN_NAME = 'owner_type')
THEN
   ALTER TABLE `engine4_hdfvrprofiler_hdfvrprofilers` ADD COLUMN owner_type VARCHAR( 128 ) NOT NULL ;
END IF;
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS
WHERE 
	TABLE_SCHEMA = dbName AND
	TABLE_NAME = 'engine4_hdfvrprofiler_hdfvrprofilers' AND 
	COLUMN_NAME = 'parent_id')
THEN
   ALTER TABLE `engine4_hdfvrprofiler_hdfvrprofilers` ADD COLUMN parent_id INT( 11 ) DEFAULT NULL;
END IF;
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS
WHERE 
	TABLE_SCHEMA = dbName AND
	TABLE_NAME = 'engine4_hdfvrprofiler_hdfvrprofilers' AND 
	COLUMN_NAME = 'parent_type')
THEN
   ALTER TABLE `engine4_hdfvrprofiler_hdfvrprofilers` ADD COLUMN parent_type VARCHAR( 128 ) DEFAULT NULL;
END IF;
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS
WHERE 
	TABLE_SCHEMA = dbName AND
	TABLE_NAME = 'engine4_hdfvrprofiler_hdfvrprofilers' AND 
	COLUMN_NAME = 'duration')
THEN
   ALTER TABLE `engine4_hdfvrprofiler_hdfvrprofilers` ADD COLUMN duration INT( 10 ) DEFAULT NULL ;
END IF;
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS
WHERE 
	TABLE_SCHEMA = dbName AND
	TABLE_NAME = 'engine4_hdfvrprofiler_hdfvrprofilers' AND 
	COLUMN_NAME = 'comment_count')
THEN
   ALTER TABLE `engine4_hdfvrprofiler_hdfvrprofilers` ADD COLUMN comment_count INT( 10 ) NOT NULL ;
END IF;
END;


CALL rename_table_hdfvr_recordings_if_exists(Database());


UPDATE `engine4_core_menuitems` SET `label` = 'Video Profile Permissions' WHERE `engine4_core_menuitems`.`label` ='Member Level HDFVR Profiler Sett';

UPDATE `engine4_core_menuitems` SET `label` = 'Video Profile Settings' WHERE `engine4_core_menuitems`.`label` = 'HDFVR Profiler Settings';