<?php 
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Hdfvrprofiler
 * @copyright  Copyright 2010 HDFVR.com
 * @license    http://avchat.net/license
 * @version    
 * @author     Mihai Frentiu <mihai@avchathq.com>
 */

/**
 * @category   Application_Extensions
 * @package    Hdfvrprofiler
 * @copyright  Copyright 2010 HDFVR.com
 * @license    http://avchat.net/license
 */
return array (
  'package' => 
  array (
    'type' => 'module',
    'name' => 'hdfvrprofiler',
    'version' => '447',
    'path' => 'application/modules/Hdfvrprofiler',
  	'title' => 'Video Profile',
    'description' => 'What do you think about having a Video Profile?',
    'author' => 'AVChat Software',
    'meta' => 
    array (
      
    ),
    'callback' => 
    array (
      'class' => 'Engine_Package_Installer_Module',
    ),
    'actions' => 
    array (
      0 => 'install',
      1 => 'upgrade',
      2 => 'refresh',
      3 => 'enable',
      4 => 'disable',
    ),
    'directories' => 
    array (
      0 => 'application/modules/Hdfvrprofiler',
    ),
    'files' => 
    array (
      0 => 'application/languages/en/hdfvrprofiler.csv',
    ),
  ),
  'items' => array(
  	'hdfvrprofiler'
  ),
); ?>