INSERT IGNORE INTO `engine4_activity_actiontypes` (`type`, `module`,  `body`,  `enabled`,  `displayable`,  `attachable`,  `commentable`,  `shareable`, `is_generated`) VALUES
('recorded_video_new', 'hdfvrprofiler', '{item:$subject} posted a new profile video:', '1', '5', '1', '3', '1', 0);

RENAME TABLE `engine4_hdfvr_recordings` TO `engine4_hdfvrprofiler_recordings`;

ALTER TABLE `engine4_hdfvrprofiler_recordings` CHANGE COLUMN id recordings_id int(10);

ALTER TABLE `engine4_hdfvrprofiler_recordings` ADD COLUMN owner_id INT( 11 ) NOT NULL;