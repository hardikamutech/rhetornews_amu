INSERT IGNORE INTO `engine4_core_menuitems` (`name`, `module`, `label`, `plugin`, `params`, `menu`, `submenu`, `order`) VALUES
('core_main_hdfvrprofiler_delete', 'hdfvrprofiler', 'Delete My Video Profile', 'Hdfvrprofiler_Plugin_Menus', '', 'user_home', '', 9);

UPDATE `engine4_core_menuitems` SET `plugin` = 'Hdfvrprofiler_Plugin_Menus' WHERE `name` ='core_main_hdfvrprofiler_view';

UPDATE `engine4_core_menuitems` SET `plugin` = 'Hdfvrprofiler_Plugin_Menus' WHERE `name` ='core_main_hdfvrprofiler';

ALTER TABLE `engine4_hdfvrprofiler_recordings` ADD COLUMN owner_type VARCHAR( 128 ) NOT NULL ;

ALTER TABLE `engine4_hdfvrprofiler_recordings` ADD COLUMN parent_id INT( 11 ) DEFAULT NULL;

ALTER TABLE `engine4_hdfvrprofiler_recordings` ADD COLUMN parent_type VARCHAR( 128 ) DEFAULT NULL;

ALTER TABLE `engine4_hdfvrprofiler_recordings` ADD COLUMN duration INT( 10 ) DEFAULT NULL ;

ALTER TABLE `engine4_hdfvrprofiler_recordings` ADD COLUMN comment_count INT( 10 ) NOT NULL ;

INSERT IGNORE INTO `engine4_authorization_permissions`
  SELECT
    level_id as `level_id`,
    'hdfvrprofiler_recording' as `type`,
    'comment' as `name`,
    1 as `value`,
    NULL as `params`
  FROM `engine4_authorization_levels` WHERE `type` IN('user');
  
INSERT IGNORE INTO `engine4_authorization_permissions`
  SELECT
    level_id as `level_id`,
    'hdfvrprofiler_recording' as `type`,
    'comment' as `name`,
    2 as `value`,
    NULL as `params`
  FROM `engine4_authorization_levels` WHERE `type` IN('moderator', 'admin');
  
INSERT IGNORE INTO `engine4_authorization_permissions`
  SELECT
    level_id as `level_id`,
    'hdfvrprofiler_recording' as `type`,
    'auth_view' as `name`,
    5 as `value`,
    '["everyone","owner_network","owner_member_member","owner_member","owner"]' as `params`
  FROM `engine4_authorization_levels` WHERE `type` NOT IN('public');
  
INSERT IGNORE INTO `engine4_authorization_permissions`
  SELECT
    level_id as `level_id`,
    'hdfvrprofiler_recording' as `type`,
    'auth_comment' as `name`,
    5 as `value`,
    '["everyone","owner_network","owner_member_member","owner_member","owner"]' as `params`
  FROM `engine4_authorization_levels` WHERE `type` NOT IN('public');
  
INSERT IGNORE INTO `engine4_authorization_permissions`
  SELECT
    level_id as `level_id`,
    'hdfvrprofiler_recording' as `type`,
    'view' as `name`,
    1 as `value`,
    NULL as `params`
  FROM `engine4_authorization_levels` WHERE `type` IN('public');  
  
INSERT IGNORE INTO `engine4_authorization_permissions`
  SELECT
    level_id as `level_id`,
    'hdfvrprofiler_recording' as `type`,
    'view' as `name`,
    2 as `value`,
    NULL as `params`
  FROM `engine4_authorization_levels` WHERE `type` IN('moderator', 'admin');
  
INSERT IGNORE INTO `engine4_authorization_permissions`
  SELECT
    level_id as `level_id`,
    'hdfvrprofiler_recording' as `type`,
    'view' as `name`,
    1 as `value`,
    NULL as `params`
  FROM `engine4_authorization_levels` WHERE `type` IN('user');

ALTER TABLE `engine4_core_menuitems` CHANGE `label` `label` VARCHAR( 40 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL; 

UPDATE `engine4_core_menuitems` SET `label` = 'Video Profile Permissions' WHERE `engine4_core_menuitems`.`label` ='Member Level HDFVR Profiler Sett';

UPDATE `engine4_core_menuitems` SET `label` = 'Video Profile Settings' WHERE `engine4_core_menuitems`.`label` = 'HDFVR Profiler Settings';