<?php

/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Hdfvrprofiler
 * @copyright  Copyright 2010 HDFVR.com
 * @license    http://avchat.net/license
 * @version
 * @author     Mihai Frentiu <mihai@avchathq.com>
 */

/**
 * @category   Application_Extensions
 * @package    Hdfvrprofiler
 * @copyright  Copyright 2010 HDFVR.com
 * @license    http://avchat.net/license
 */
/*
  {"route":"default","module":"hdfvrprofiler", "action":"delete", "class":"smoothbox" , "icon":"application/modules/Hdfvrprofiler/externals/images/links/showreel.png"}
 */
class Hdfvrprofiler_Plugin_Menus {

    public function getPermisionsHdfvrprofiler($level_id) {
        //getPermisionsHdfvrprofiler
        //------------------------------
        //Get HDFVR Profiler Permissions
        //------------------------------
        $form = new Hdfvrprofiler_Form_Admin_Level_Edit();
        $permissionsTable = Engine_Api::_()->getDbtable('permissions', 'authorization');
        return $permissionsTable->getAllowed('hdfvrprofiler', $level_id, array_keys($form->getValues()));
    }

    public function onMenuInitialize_CoreMainHdfvrprofilerViewOther() {

        $viewer = Engine_Api::_()->user()->getViewer();
        $subject = Engine_Api::_()->core()->getSubject();

        // Not logged in
        if (!$viewer->getIdentity() || $viewer->getGuid(false) === $subject->getGuid(false)) {
            return false;
        }

        //--------------------------
        //Get level id
        //--------------------------
        $level_id = $subject->level_id;

        $level_hdfvrprofiler_permissions = $this->getPermisionsHdfvrprofiler($level_id);

        //var_dump($level_hdfvrprofiler_permissions['hdfvr_allow_rec']);
        if ($level_hdfvrprofiler_permissions['hdfvr_allow_rec']) {


            $hisRecordings = Engine_Api::_()->hdfvrprofiler()->getSubjectVideoProfile($subject->getIdentity());
            if (count($hisRecordings) > 0) {
                return array(
                    'label' => 'View Video',
                    'icon' => 'application/modules/Hdfvrprofiler/externals/images/links/showreel.png',
                    'class' => 'smoothbox',
                    'module' => 'hdfvrprofiler',
                    'route' => 'default',
                    'params' => array(
                        'controller' => 'index',
                        'action' => 'view_friend',
                        'user_id' => $subject->getIdentity()
                    ),
                );
            }
        }
        return false;
    }

    public function onMenuInitialize_CoreMainHdfvrprofilerDelete() {
        $viewer = Engine_Api::_()->user()->getViewer();

        // Not logged in
        if (!$viewer->getIdentity()) {
            return false;
        }
        //--------------------------
        //Get level id
        //--------------------------
        $level_id = $viewer->level_id;

        $level_hdfvrprofiler_permissions = $this->getPermisionsHdfvrprofiler($level_id);

        if ($level_hdfvrprofiler_permissions['hdfvr_allow_rec']) {
            $myRecordings = Engine_Api::_()->hdfvrprofiler()->getSubjectVideoProfile($viewer->getIdentity());

            if (count($myRecordings) > 0) {
                return array(
                    'label' => 'Delete My Video Profile',
                    'icon' => 'application/modules/Hdfvrprofiler/externals/images/links/delete.png',
                    'class' => 'smoothbox',
                    'module' => 'hdfvrprofiler',
                    'route' => 'default',
                    'params' => array(
                        'controller' => 'index',
                        'action' => 'delete'),
                );
            }
        }
        return false;
    }

    public function onMenuInitialize_CoreMainHdfvrprofilerView() {
        $viewer = Engine_Api::_()->user()->getViewer();

        // Not logged in
        if (!$viewer->getIdentity()) {
            return false;
        }

        //--------------------------
        //Get level id
        //--------------------------
        $level_id = $viewer->level_id;

        $level_hdfvrprofiler_permissions = $this->getPermisionsHdfvrprofiler($level_id);

        if ($level_hdfvrprofiler_permissions['hdfvr_allow_rec']) {
            $myRecordings = Engine_Api::_()->hdfvrprofiler()->getSubjectVideoProfile($viewer->getIdentity());

            if (count($myRecordings) > 0) {
                return array(
                    'label' => 'View My Video Profile',
                    'icon' => 'application/modules/Hdfvrprofiler/externals/images/links/showreel.png',
                    'class' => 'smoothbox',
                    'module' => 'hdfvrprofiler',
                    'route' => 'default',
                    'params' => array(
                        'controller' => 'index',
                        'action' => 'view'),
                );
            }
        }
        return false;
    }

    public function onMenuInitialize_CoreMainHdfvrprofiler() {
        $viewer = Engine_Api::_()->user()->getViewer();

        // Not logged in
        if (!$viewer->getIdentity()) {
            return false;
        }

        //--------------------------
        //Get level id
        //--------------------------
        $level_id = $viewer->level_id;

        $level_hdfvrprofiler_permissions = $this->getPermisionsHdfvrprofiler($level_id);

        if ($level_hdfvrprofiler_permissions['hdfvr_allow_rec']) {

            return array(
                'label' => 'Record Video Profile',
                'icon' => 'application/modules/Hdfvrprofiler/externals/images/links/camera.png',
                'class' => 'smoothbox',
                'module' => 'hdfvrprofiler',
                'route' => 'default',
                'params' => array(
                    'controller' => 'index'),
            );
        }
        return false;
    }

}

