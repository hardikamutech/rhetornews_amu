<?php

/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Hdfvrprofiler
 * @copyright  Copyright 2010 HDFVR.com
 * @license    http://avchat.net/license
 * @version
 * @author     Mihai Frentiu <mihai@avchathq.com>
 */

/**
 * @category   Application_Extensions
 * @package    Hdfvrprofiler
 * @copyright  Copyright 2010 HDFVR.com
 * @license    http://avchat.net/license
 */
class Hdfvrprofiler_Form_Recorder extends Engine_Form {

    public function init() {
        //$this->setTitle('Record Your Profile Video');
        //$this->setDescription('');
        $this->setAttrib('class', 'global_form_popup');

        $this->addElement('Hidden', 'stream_name', array(
            'order' => 990,
            'value' => ''
        ));
        $this->addElement('Hidden', 'stream_duration', array(
            'order' => 991,
            'value' => ''
        ));


        // Init submit
        $this->addElement('Button', 'submit', array(
            'label' => 'Add Video as Comment',
            'type' => 'submit',
            'ignore' => true,
            'style' => 'display:none',
        ));

        // Set default action
        $this->setAction(Zend_Controller_Front::getInstance()->getRouter()->assemble(array(
                    'module' => 'hdfvrprofiler',
                    'controller' => 'index',
                    'action' => 'index',
                        ), 'default'));
    }

}

?>