<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Hdfvrprofiler
 * @copyright  Copyright 2010 HDFVR.com
 * @license    http://avchat.net/license
 * @version    
 * @author     Mihai Frentiu <mihai@avchathq.com>
 */

/**
 * @category   Application_Extensions
 * @package    Hdfvrprofiler
 * @copyright  Copyright 2010 HDFVR.com
 * @license    http://avchat.net/license
 */
class Hdfvrprofiler_Form_Admin_Level_Edit extends Engine_Form
{
  public function init()
  {
  	$this
      ->setTitle(' Video Profile Member Level Permissions')
      ->setDescription("");

    $this->loadDefaultDecorators();
    $this->getDecorator('Description')->setOptions(array('tag' => 'h4', 'placement' => 'PREPEND'));
    
    // prepare user levels
    $table = Engine_Api::_()->getDbtable('levels', 'authorization');
    $select = $table->select();
    $user_levels = $table->fetchAll($select);

    foreach ($user_levels as $user_level) {
      $levels_prepared[$user_level->level_id] = $user_level->getTitle();
    }

    // category field
    $this->addElement('Select', 'level_id', array(
      'label' => 'Member Level',
      'multiOptions' => $levels_prepared,
      'onchange' => 'javascript:fetchLevelSettings(this.value);',
      'ignore' => true,
    ));
    
    $this->addElement('Radio', 'hdfvr_allow_rec', array(
      'label' => 'Allow Users from this Level to Record Video Profiles',
      'required' => true,
      'multiOptions' => array(
        1 => 'Yes, allow video profiles',
        0 => 'No, do not allow video profiles'
      ),
      'value' => 1
    ));
    
    $this->addElement('Text', 'hdfvr_rectime', array(
      'label' => 'Video Profile recording time limit',
      'description' => 'The ammount of time (in minutes) that a user can record. For unlimited time set it to -1.'
    ));
    
    $this->addElement('Radio', 'view', array(
    		'label' => 'Allow Users from this Level to View the Video Profiles',
    		'required' => true,
    		'multiOptions' => array(
    				1 => 'Yes, allow viewing video profiles',
    				0 => 'No, do not allow viewing video profiles'
    		),
    		'value' => 1
    ));
    
    $this->addElement('Radio', 'comment', array(
    		'label' => 'Allow Users from this Level to Comment on Video Profiles',
    		'required' => true,
    		'multiOptions' => array(
    				1 => 'Yes, allow commenting on video profiles',
    				0 => 'No, do not allow commenting on video profiles'
    		),
    		'value' => 1
    ));
    
    // Add submit
    $this->addElement('Button', 'submit', array(
      'label' => 'Save Changes',
      'type' => 'submit',
      'ignore' => true,
    ));
    
  }
}