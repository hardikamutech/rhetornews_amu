<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Hdfvrprofiler
 * @copyright  Copyright 2010 HDFVR.com
 * @license    http://avchat.net/license
 * @version    
 * @author     Mihai Frentiu <mihai@avchathq.com>
 */

/**
 * @category   Application_Extensions
 * @package    Hdfvrprofiler
 * @copyright  Copyright 2010 HDFVR.com
 * @license    http://avchat.net/license
 */
class Hdfvrprofiler_Form_Admin_Settings_General extends Engine_Form
{
  public function init()
  {
    // Set form attributes
    $this->setTitle('General Setings for the Video Profile Plugin');
    $this->setDescription('These settings affect the Video Profile plugin');
    
    
    

    $this->addElement('Text', 'rtmp_connectionstring', array(
      'label' => 'RTMP Connection String',
      'description' => 'The RTMP connection string to the hdfvr application on your media server.Example: "rtmp://my-media-server.com/hdfvr/_definst_"',
    ));    
    
    $this->addElement('Text', 'buffer_size', array(
      'label' => 'Buffer',
      'description' => 'Specifies how long the buffer for the outgoing audio/video data can grow before Flash Player starts dropping frames. On a high-speed connection, buffer time will not affect anything because data is sent almost as quickly as it is captured and there is no need to buffer it. On a slow connection, however, there might be a significant difference between how fast Flash Player can capture audiovideo data data and how fast it can be sent to the client, thus the surplus needs to b buffered.'
    ));
    
    $this->addElement('Text', 'playback_buffer', array(
      'label' => 'Playback buffer',
      'description' => 'Accepted values 0-60.'
    ));
    
    $quality_profiles = array();
    $quality_profiles['wide_1280x720x30x90'] = 'HD 720p';
    $quality_profiles['wide_640x360x30x90'] = 'Wide - 640x360';
    $quality_profiles['wide_864x480x30x90'] = 'Wide - 864x480';
    $quality_profiles['normal_640x480x30x90'] = 'Normal - 640x480';
	$quality_profiles['normal_400x300x30x90'] = 'Normal - 400x300';
	$quality_profiles['normal_320x240x30x90'] = 'Normal - 320x240';
    
    
    // category field
    $this->addElement('Select', 'quality_profile', array(
      'label' => 'Video Quality Profile',
      'multiOptions' => $quality_profiles,
    ));
    
    
    // init submit
    $this->addElement('Button', 'submit', array(
      'label' => 'Save Changes',
      'type' => 'submit',
      'ignore' => true,
    ));
  }
}