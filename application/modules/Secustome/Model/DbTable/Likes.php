<?php

/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Hdfvrprofiler
 * @copyright  Copyright 2010 HDFVR.com
 * @license    http://avchat.net/license
 * @version
 * @author     Mihai Frentiu <mihai@avchathq.com>
 */

/**
 * @category   Application_Extensions
 * @package    Rssnews
 * @copyright  Copyright 2010 HDFVR.com
 * @license    http://avchat.net/license
 */
class Secustome_Model_DbTable_Likes extends Engine_Db_Table {

    protected $_rowClass = 'Secustome_Model_Likes';

    const resource_type_rss_like = 'rss_like';

}

