<?php

require APPLICATION_PATH_LIB . '/AmuController.php';

class Socialapi_IndexController extends Socialapi_AmuController {

    protected $_formClass = 'User_Form_Signup_Fields';
    protected $_elements = array();

    public function indexAction() {

    }

    public function loginAction() {
        $post = $this->getRequest()->getPost();

        $email = $post['email'];
        $password = $post['password'];

        $user_table = Engine_Api::_()->getDbtable('users', 'user');
        $user_select = $user_table->select()
                ->where('email = ?', $email);          // If post exists
        $user = $user_table->fetchRow($user_select);

        // Get ip address
        $db = Engine_Db_Table::getDefaultAdapter();
        $ipObj = new Engine_IP();
        $ipExpr = new Zend_Db_Expr($db->quoteInto('UNHEX(?)', bin2hex($ipObj->toBinary())));

        // Check if user exists
        if (empty($user)) {
            //$this->view->status = false;
            //$this->view->error = Zend_Registry::get('Zend_Translate')->_('No record of a member with that email was found.');
            //$form->addError(Zend_Registry::get('Zend_Translate')->_('No record of a member with that email was found.'));
            // Register login
            Engine_Api::_()->getDbtable('logins', 'user')->insert(array(
                'email' => $email,
                'ip' => $ipExpr,
                'timestamp' => new Zend_Db_Expr('NOW()'),
                'state' => 'no-member',
            ));

            $jArray = array("status" => "false", "message" => "No record of a member with that email was found.");
            $this->jsonResponse($jArray, 200);
            exit;
        } else {
            // Check if user is verified and enabled
            if (!$user->enabled) {
                if (!$user->verified) {
//                    $this->view->status = false;
//
//                    $resend_url = $this->_helper->url->url(array('action' => 'resend', 'email' => $email), 'user_signup', true);
//                    $translate = Zend_Registry::get('Zend_Translate');
//                    $error = $translate->translate('This account still requires either email verification.');
//                    $error .= ' ';
//                    $error .= sprintf($translate->translate('Click <a href="%s">here</a> to resend the email.'), $resend_url);
//                    $form->getDecorator('errors')->setOption('escape', false);
//                    $form->addError($error);
                    // Register login
                    Engine_Api::_()->getDbtable('logins', 'user')->insert(array(
                        'user_id' => $user->getIdentity(),
                        'email' => $email,
                        'ip' => $ipExpr,
                        'timestamp' => new Zend_Db_Expr('NOW()'),
                        'state' => 'disabled',
                    ));

                    $jArray = array("status" => "false", "message" => "This account still requires either email verification");
                    $this->jsonResponse($jArray, 200);
                    exit;
                } else if (!$user->approved) {
//                    $this->view->status = false;
//
//                    $translate = Zend_Registry::get('Zend_Translate');
//                    $error = $translate->translate('This account still requires admin approval.');
//                    $form->getDecorator('errors')->setOption('escape', false);
//                    $form->addError($error);
                    // Register login
                    Engine_Api::_()->getDbtable('logins', 'user')->insert(array(
                        'user_id' => $user->getIdentity(),
                        'email' => $email,
                        'ip' => $ipExpr,
                        'timestamp' => new Zend_Db_Expr('NOW()'),
                        'state' => 'disabled',
                    ));

                    $jArray = array("status" => "false", "message" => "This account still requires admin approval");
                    $this->jsonResponse($jArray, 200);
                    exit;
                }
            } else {

                $authResult = Engine_Api::_()->user()->authenticate($email, $password);
                $authCode = $authResult->getCode();
                Engine_Api::_()->user()->setViewer();
                if ($authCode != Zend_Auth_Result::SUCCESS) {
//                    $this->view->status = false;
//                    $this->view->error = Zend_Registry::get('Zend_Translate')->_('Invalid credentials');
//                    $form->addError(Zend_Registry::get('Zend_Translate')->_('Invalid credentials supplied'));
                    // Register login
                    Engine_Api::_()->getDbtable('logins', 'user')->insert(array(
                        'user_id' => $user->getIdentity(),
                        'email' => $email,
                        'ip' => $ipExpr,
                        'timestamp' => new Zend_Db_Expr('NOW()'),
                        'state' => 'bad-password',
                    ));

                    $jArray = array("status" => "false", "message" => "Invalid credentials supplied");
                    $this->jsonResponse($jArray, 200);
                    exit;
                } else {

                    $loginTable = Engine_Api::_()->getDbtable('logins', 'user');
                    $loginTable->insert(array(
                        'user_id' => $user->getIdentity(),
                        'email' => $email,
                        'ip' => $ipExpr,
                        'timestamp' => new Zend_Db_Expr('NOW()'),
                        'state' => 'success',
                        'active' => true,
                    ));

                    //get field value of users
                    $viewer = Engine_Api::_()->user()->getViewer();
                    $fields = Engine_Api::_()->fields()->getFieldsValuesByAlias($viewer);

                    //
                    $user_data = array(
                        "user_id" => $user->user_id,
                        "email" => $user->email,
                        "username" => $user->username,
                        "displayname" => $user->displayname,
                        "birthdate" => $fields['birthdate'],
                        "gender" => $fields['gender'],
                        "photo_id" => $user->photo_id,
                        "photo_url" => $user->getPhotoUrl()
                    );

                    $jArray = array("status" => "true", "message" => "Logged in Successfull!", "data" => $user_data);
                    $this->jsonResponse($jArray, 200);
                    exit;
                }
            }
        }
    }

    public function signupAction() {
        //$this_form = new Core_Plugin_FormSequence_Abstract;
        $post = $this->getRequest()->getPost();
        $email = $post['email'];
        $password = $post['password'];
        $passconf = $password;
        $username = $post['username'];
        $profile_type = "";
        $timezone = $post['timezone'];
        $language = $post['language'];
        $name = "";
        $terms = $post['terms'];
        $accountArr = array(
            "name" => $name,
            "email" => $email,
            "password" => $password,
            "passconf" => $passconf,
            "username" => $username,
            "profile_type" => $profile_type,
            "timezone" => $timezone,
            "language" => $language,
            "terms" => $terms
        );
        $user_table = Engine_Api::_()->getDbtable('users', 'user');
        $user_select = $user_table->select()
                ->where('email = ?', $email)
                ->where('username = ?', $username);          // If post exists
        $userData = $user_table->fetchRow($user_select);

        if (empty($userData)) {
            //first step start
            $settings = Engine_Api::_()->getApi('settings', 'core');
            $random = ($settings->getSetting('user.signup.random', 0) == 1);
            $emailadmin = ($settings->getSetting('user.signup.adminemail', 0) == 1);
            if ($emailadmin) {
                // the signup notification is emailed to the first SuperAdmin by default
                $users_table = Engine_Api::_()->getDbtable('users', 'user');
                $users_select = $users_table->select()
                        ->where('level_id = ?', 1)
                        ->where('enabled >= ?', 1);
                $super_admin = $users_table->fetchRow($users_select);
            }
            $data = $accountArr;

            // Add email and code to invite session if available
            $inviteSession = new Zend_Session_Namespace('invite');

            if (isset($data['email'])) {
                $inviteSession->signup_email = $data['email'];
            }
            if (isset($data['code'])) {
                $inviteSession->signup_code = $data['code'];
            }

            if ($random) {
                $data['password'] = Engine_Api::_()->user()->randomPass(10);
            }

            if (isset($data['language'])) {
                $data['locale'] = $data['language'];
            }


// Create user
            // Note: you must assign this to the registry before calling save or it
            // will not be available to the plugin in the hook


            $this->_registry->user = $user = Engine_Api::_()->getDbtable('users', 'user')->createRow();
            $user->setFromArray($data);
            $user->save();
            Engine_Api::_()->user()->setViewer($user);

            // Increment signup counter
            Engine_Api::_()->getDbtable('statistics', 'core')->increment('user.creations');

            if ($user->verified && $user->enabled) {
                // Create activity for them
                Engine_Api::_()->getDbtable('actions', 'activity')->addActivity($user, $user, 'signup');
                // Set user as logged in if not have to verify email
                Engine_Api::_()->user()->getAuth()->getStorage()->write($user->getIdentity());
            }

            $mailType = null;
            $mailParams = array(
                'host' => $_SERVER['HTTP_HOST'],
                'email' => $user->email,
                'date' => time(),
                'recipient_title' => $user->getTitle(),
                'recipient_link' => $user->getHref(),
                'recipient_photo' => $user->getPhotoUrl('thumb.icon'),
                'object_link' => Zend_Controller_Front::getInstance()->getRouter()->assemble(array(), 'user_login', true),
            );

            // Add password to email if necessary
            if ($random) {
                $mailParams['password'] = $data['password'];
            }

            // Mail stuff
            switch ($settings->getSetting('user.signup.verifyemail', 0)) {
                case 0:
                    // only override admin setting if random passwords are being created
                    if ($random) {
                        $mailType = 'core_welcome_password';
                    }
                    if ($emailadmin) {
                        $mailAdminType = 'notify_admin_user_signup';
                        $siteTimezone = Engine_Api::_()->getApi('settings', 'core')->getSetting('core.locale.timezone', 'America/Los_Angeles');
                        $date = new DateTime("now", new DateTimeZone($siteTimezone));
                        $mailAdminParams = array(
                            'host' => $_SERVER['HTTP_HOST'],
                            'email' => $user->email,
                            'date' => $date->format('F j, Y, g:i a'),
                            'recipient_title' => $super_admin->displayname,
                            'object_title' => $user->displayname,
                            'object_link' => $user->getHref(),
                        );
                    }
                    break;

                case 1:
                    // send welcome email
                    $mailType = ($random ? 'core_welcome_password' : 'core_welcome');
                    if ($emailadmin) {
                        $mailAdminType = 'notify_admin_user_signup';
                        $siteTimezone = Engine_Api::_()->getApi('settings', 'core')->getSetting('core.locale.timezone', 'America/Los_Angeles');
                        $date = new DateTime("now", new DateTimeZone($siteTimezone));
                        $mailAdminParams = array(
                            'host' => $_SERVER['HTTP_HOST'],
                            'email' => $user->email,
                            'date' => $date->format('F j, Y, g:i a'),
                            'recipient_title' => $super_admin->displayname,
                            'object_title' => $user->getTitle(),
                            'object_link' => $user->getHref(),
                        );
                    }
                    break;

                case 2:
                    // verify email before enabling account
                    $verify_table = Engine_Api::_()->getDbtable('verify', 'user');
                    $verify_row = $verify_table->createRow();
                    $verify_row->user_id = $user->getIdentity();
                    $verify_row->code = md5($user->email
                            . $user->creation_date
                            . $settings->getSetting('core.secret', 'staticSalt')
                            . (string) rand(1000000, 9999999));
                    $verify_row->date = $user->creation_date;
                    $verify_row->save();

                    $mailType = ($random ? 'core_verification_password' : 'core_verification');

                    $mailParams['object_link'] = Zend_Controller_Front::getInstance()->getRouter()->assemble(array(
                        'action' => 'verify',
                        'email' => $user->email,
                        'verify' => $verify_row->code
                            ), 'user_signup', true);

                    if ($emailadmin) {
                        $mailAdminType = 'notify_admin_user_signup';

                        $mailAdminParams = array(
                            'host' => $_SERVER['HTTP_HOST'],
                            'email' => $user->email,
                            'date' => date("F j, Y, g:i a"),
                            'recipient_title' => $super_admin->displayname,
                            'object_title' => $user->getTitle(),
                            'object_link' => $user->getHref(),
                        );
                    }
                    break;

                default:
                    // do nothing
                    break;
            }

//        if (!empty($mailType)) {
//            $this->_registry->mailParams = $mailParams;
//            $this->_registry->mailType = $mailType;
//            // Moved to User_Plugin_Signup_Fields
//            // Engine_Api::_()->getApi('mail', 'core')->sendSystem(
//            //   $user,
//            //   $mailType,
//            //   $mailParams
//            // );
//        }
//
//        if (!empty($mailAdminType)) {
//            $this->_registry->mailAdminParams = $mailAdminParams;
//            $this->_registry->mailAdminType = $mailAdminType;
//            // Moved to User_Plugin_Signup_Fields
//            // Engine_Api::_()->getApi('mail', 'core')->sendSystem(
//            //   $user,
//            //   $mailType,
//            //   $mailParams
//            // );
//        }
            // first step end
            //second step start
            $firstname = $post['firstname'];
            $lastname = $post['lastname'];
            $gender = $post['gender'];
            $birthday = $post['birthday'];
            $website = $post['website'];
            $twitter = $post['twitter'];
            $facebook = $post['facebook'];
            $AIM = $post['AIM'];
            $about_me = $post['about_me'];
            $bday = explode('-', $birthday);

            $fieldDataArr = array(
                '1' => 1,
                '3' => $firstname,
                '4' => $lastname,
                '5' => $gender,
                "6" => array("month" => $bday[0], "day" => $bday[1], "year" => $bday[2]),
                "8" => $website,
                "9" => $twitter,
                "10" => $facebook,
                "11" => $AIM,
                "13" => $about_me
            );
            $user = $this->_registry->user;
            $this->getSession()->data = $fieldDataArr;
            $profileTypeField = $this->getProfileTypeField();

            if ($profileTypeField) {
                $accountSession = new Zend_Session_Namespace('User_Plugin_Signup_Account');

                $profileTypeValue = @$accountSession->data['profile_type'];
                if ($profileTypeValue) {

                    $values = Engine_Api::_()->fields()->getFieldsValues($user);
                    $valueRow = $values->createRow();
                    $valueRow->field_id = $profileTypeField->field_id;
                    $valueRow->item_id = $user->getIdentity();
                    $valueRow->value = $profileTypeValue;
                    $valueRow->save();
                } else {

                    $topStructure = Engine_Api::_()->fields()->getFieldStructureTop('user');
                    if (count($topStructure) == 1 && $topStructure[0]->getChild()->type == 'profile_type') {
                        $profileTypeField = $topStructure[0]->getChild();

                        $options = $profileTypeField->getOptions();

                        if (count($options) == 1) {

                            $values = Engine_Api::_()->fields()->getFieldsValues($user);
                            $valueRow = $values->createRow();
                            $valueRow->field_id = $profileTypeField->field_id;
                            $valueRow->item_id = $user->getIdentity();
                            $valueRow->value = $options[0]->option_id;
                            $valueRow->save();
                        }
                    }
                }
            }

            // Save them values
//            $form = $this->getForm()->setItem($user);
//            $form->setProcessedValues($this->getSession()->data);
//            $form->saveValues();
            //$aliasValues = Engine_Api::_()->fields()->getFieldsValuesByAlias($user);
            $field_arr = array(
                "profile_type" => 1,
                "first_name" => $firstname,
                "last_name" => $lastname,
                "gender" => $gender,
                "birthdate" => $birthday
            );
//            echo "<pre>";
//            print_r($field_arr);
//            exit;
            $user->setDisplayName($field_arr);
            $user->save();

//        // Send Welcome E-mail
//        if (isset($this->_registry->mailType) && $this->_registry->mailType) {
//            $mailType = $this->_registry->mailType;
//            $mailParams = $this->_registry->mailParams;
//            Engine_Api::_()->getApi('mail', 'core')->sendSystem(
//                    $user, $mailType, $mailParams
//            );
//        }
//
//        // Send Notify Admin E-mail
//        if (isset($this->_registry->mailAdminType) && $this->_registry->mailAdminType) {
//            $mailAdminType = $this->_registry->mailAdminType;
//            $mailAdminParams = $this->_registry->mailAdminParams;
//            Engine_Api::_()->getApi('mail', 'core')->sendSystem(
//                    $user, $mailAdminType, $mailAdminParams
//            );
//        }
//
//        //end second steps
            //third steps start
            if (isset($_FILES['profile_image'])) {

                $uploads_dir = APPLICATION_PATH . '/public/temporary/';

                $file_name = $_FILES['profile_image']['name'];
                $file_size = $_FILES['profile_image']['size'];
                $file_tmp = $_FILES['profile_image']['tmp_name'];
                $file_type = $_FILES['profile_image']['type'];
                $file_ext = strtolower(end(explode('.', $_FILES['profile_image']['name'])));
                $file_resize_name = APPLICATION_PATH . "/public/temporary\ " . $file_name;
                $file_resize_file_name = str_replace(' ', '', $file_resize_name);
                //$string = preg_replace('/\s+/', '', $string);

                $error = $_FILES['profile_image']['error'];
                if ($error == 0) {
                    $tmp_name = $file_tmp;
                    $name = $file_name;
                    move_uploaded_file($tmp_name, "$uploads_dir/$name");
                    $this->resizeImages($file_resize_file_name);
                }
            }
            $params = array(
                'parent_type' => 'user',
                'parent_id' => $user->user_id
            );
            if (!empty($this->getSession()->tmp_file_id)) {
                // Save

                $storage = Engine_Api::_()->getItemTable('storage_file');

                // Update info
                $iMain = $storage->getFile($this->getSession()->tmp_file_id);
                $iMain->setFromArray($params);
                $iMain->save();
                $iMain->updatePath();

                $iProfile = $storage->getFile($this->getSession()->tmp_file_id, 'thumb.profile');
                $iProfile->setFromArray($params);
                $iProfile->save();
                $iProfile->updatePath();

                $iNormal = $storage->getFile($this->getSession()->tmp_file_id, 'thumb.normal');
                $iNormal->setFromArray($params);
                $iNormal->save();
                $iNormal->updatePath();

                $iSquare = $storage->getFile($this->getSession()->tmp_file_id, 'thumb.icon');
                $iSquare->setFromArray($params);
                $iSquare->save();
                $iSquare->updatePath();

                // Update row

                $user->photo_id = $iMain->file_id;
                $user->save();
            }

            //end third steps
            //get field value of users

            $user_data = array(
                "user_id" => $user->user_id,
                "email" => $user->email,
                "username" => $user->username,
                "displayname" => $user->displayname,
                "birthdate" => $birthday,
                "gender" => $gender,
                "photo_id" => $user->photo_id,
                "website" => $website,
                "facebook" => $facebook,
                "twitter" => $twitter,
                "aim" => $AIM,
                "photo_url" => $user->getPhotoUrl()
            );

            $jArray = array("status" => "true", "message" => "Register Successfull!", "data" => $user_data);
            $this->jsonResponse($jArray, 200);
            exit;
        } else {
            $jArray = array("status" => "false", "message" => " Already has member in this site with this credencial. Please choose another");
            $this->jsonResponse($jArray, 200);
            exit;
        }
    }

//    public function setItem($item, $id = null) {
//        $item = $this->_normalizeItem($item, $id);
//        $this->_items[$item['id']] = $item['data'];
//        return $this;
//    }
//
//    public function getForm() {
//        $this_form = new Core_Plugin_FormSequence_Abstract;
//        if (is_null($this_form->_form)) {
//            $formArgs = array();
//            // Preload profile type field stuff
//            $profileTypeField = $this->getProfileTypeField();
//
//
//            if ($profileTypeField) {
//                $accountSession = new Zend_Session_Namespace('User_Plugin_Signup_Account');
//                $profileTypeValue = @$accountSession->data['profile_type'];
//                if ($profileTypeValue) {
//                    $formArgs = array(
//                        'topLevelId' => $profileTypeField->field_id,
//                        'topLevelValue' => $profileTypeValue,
//                    );
//                } else {
//                    $topStructure = Engine_Api::_()->fields()->getFieldStructureTop('user');
//                    if (count($topStructure) == 1 && $topStructure[0]->getChild()->type == 'profile_type') {
//                        $profileTypeField = $topStructure[0]->getChild();
//                        $options = $profileTypeField->getOptions();
//                        if (count($options) == 1) {
//                            $formArgs = array(
//                                'topLevelId' => $profileTypeField->field_id,
//                                'topLevelValue' => $options[0]->option_id,
//                            );
//                        }
//                    }
//                }
//            }
//
//            // Create form
////            Engine_Loader::loadClass($this->_formClass);
////            $class = $this->_formClass;
////            $this->_form = new $class($formArgs);
//            $data = $this->getSession()->data;
//
//
////            if (!empty($_SESSION['facebook_signup'])) {
////                try {
////                    $facebookTable = Engine_Api::_()->getDbtable('facebook', 'user');
////                    $facebook = $facebookTable->getApi();
////                    $settings = Engine_Api::_()->getDbtable('settings', 'core');
////                    if ($facebook && $settings->core_facebook_enable) {
////                        // Load Faceboolk data
////                        $apiInfo = $facebook->api('/me');
////                        $fb_data = array();
////                        $fb_keys = array('first_name', 'last_name', 'birthday', 'birthdate');
////                        foreach ($fb_keys as $key) {
////                            if (isset($apiInfo[$key])) {
////                                $fb_data[$key] = $apiInfo[$key];
////                            }
////                        }
////                        if (isset($apiInfo['birthday']) && !empty($apiInfo['birthday'])) {
////                            $fb_data['birthdate'] = date("Y-m-d", strtotime($fb_data['birthday']));
////                        }
////
////                        // populate fields, using Facebook data
////                        $struct = $this->_form->getFieldStructure();
////                        foreach ($struct as $fskey => $map) {
////                            $field = $map->getChild();
////                            if ($field->isHeading())
////                                continue;
////
////                            if (isset($field->type) && in_array($field->type, $fb_keys)) {
////                                $el_key = $map->getKey();
////                                $el_val = $fb_data[$field->type];
////                                $el_obj = $this->_form->getElement($el_key);
////                                if ($el_obj instanceof Zend_Form_Element &&
////                                        !$el_obj->getValue()) {
////                                    $el_obj->setValue($el_val);
////                                }
////                            }
////                        }
////                    }
////                } catch (Exception $e) {
////                    // Silence?
////                }
////            }
////
////            // Attempt to preload information
////            if (!empty($_SESSION['janrain_signup']) && !empty($_SESSION['janrain_signup_info'])) {
////                try {
////                    $settings = Engine_Api::_()->getDbtable('settings', 'core');
////                    if ($settings->core_janrain_enable) {
////                        $jr_info = $_SESSION['janrain_signup_info'];
////                        $jr_poco = @$_SESSION['janrain_signup_info']['merged_poco'];
////                        $jr_data = array();
////                        if (!empty($jr_info['displayName'])) {
////                            if (false !== strpos($jr_info['displayName'], ' ')) {
////                                list($jr_data['first_name'], $jr_data['last_name']) = explode(' ', $jr_info['displayName']);
////                            } else {
////                                $jr_data['first_name'] = $jr_info['displayName'];
////                            }
////                        }
////                        if (!empty($jr_info['name']['givenName'])) {
////                            $jr_data['first_name'] = $jr_info['name']['givenName'];
////                        }
////                        if (!empty($jr_info['name']['familyName'])) {
////                            $jr_data['last_name'] = $jr_info['name']['familyName'];
////                        }
////                        if (!empty($jr_info['email'])) {
////                            $jr_data['email'] = $jr_info['email'];
////                        }
////                        if (!empty($jr_info['url'])) {
////                            $jr_data['website'] = $jr_info['url'];
////                        }
////                        if (!empty($jr_info['birthday'])) {
////                            $jr_data['birthdate'] = date("Y-m-d", strtotime($jr_info['birthday']));
////                        }
////
////                        if (!empty($jr_poco['url']) && false !== stripos($jr_poco['url'], 'www.facebook.com/profile.php?id=')) {
////                            list($null, $jr_data['facebook']) = explode('www.facebook.com/profile.php?id=', $jr_poco['url']);
////                        } else if (!empty($jr_data['url']) && false !== stripos($jr_poco['url'], 'http://www.facebook.com/')) {
////                            list($null, $jr_data['facebook']) = explode('http://www.facebook.com/', $jr_data['url']);
////                        }
////                        if (!empty($jr_poco['currentLocation']['formatted'])) {
////                            $jr_data['location'] = $jr_poco['currentLocation']['formatted'];
////                        }
////                        if (!empty($jr_poco['religion'])) {
////                            // Might not match any values
////                            $jr_data['religion'] = str_replace(' ', '_', strtolower($jr_poco['religion']));
////                        }
////                        if (!empty($jr_poco['relationshipStatus'])) {
////                            // Might not match all values
////                            $jr_data['relationship_status'] = str_replace(' ', '_', strtolower($jr_poco['relationshipStatus']));
////                        }
////                        if (!empty($jr_poco['politicalViews'])) {
////                            // Only works if text
////                            $jr_data['political_views'] = $jr_poco['politicalViews'];
////                        }
////
////                        // populate fields, using janrain data
////                        $struct = $this->_form->getFieldStructure();
////                        foreach ($struct as $fskey => $map) {
////                            $field = $map->getChild();
////                            if ($field->isHeading())
////                                continue;
////
////                            if (!empty($field->type) && !empty($jr_data[$field->type])) {
////                                $val = $jr_data[$field->type];
////                            } else if (!empty($field->alias) && !empty($jr_data[$field->alias])) {
////                                $val = $jr_data[$field->alias];
////                            } else {
////                                continue;
////                            }
////
////                            $el_key = $map->getKey();
////                            $el_val = $val;
////                            $el_obj = $this->_form->getElement($el_key);
////                            if ($el_obj instanceof Zend_Form_Element &&
////                                    !$el_obj->getValue()) {
////                                $el_obj->setValue($el_val);
////                            }
////                        }
////                    }
////                } catch (Exception $e) {
////                    echo $e;
////                    // Silence?
////                }
////            }
//
//            if (!empty($data)) {
//                foreach ($data as $key => $val) {
//                    $el = $this_form->_form->getElement($key);
//                    if ($el instanceof Zend_Form_Element) {
//                        $el->setValue($val);
//                    }
//                }
//            }
//        }
//
//        return $this_form->_form;
//    }
//    public function getElement($name) {
//        if (array_key_exists($name, $this->_elements)) {
//            return $this->_elements[$name];
//        }
//        return null;
//    }

    public function getProfileTypeField() {
        $topStructure = Engine_Api::_()->fields()->getFieldStructureTop('user');
        if (count($topStructure) == 1 && $topStructure[0]->getChild()->type == 'profile_type') {
            return $topStructure[0]->getChild();
        }
        return null;
    }

    public function resizeImages($file) {

        $name = basename($file);
        $path = dirname($file);

        // Resize image (main)
        $iMainPath = $path . '/m_' . $name;
        $image = Engine_Image::factory();
        $image->open($file)
                ->autoRotate()
                ->resize(720, 720)
                ->write($iMainPath)
                ->destroy();

        // Resize image (profile)
        $iProfilePath = $path . '/p_' . $name;
        $image = Engine_Image::factory();
        $image->open($file)
                ->autoRotate()
                ->resize(200, 400)
                ->write($iProfilePath)
                ->destroy();

        // Resize image (icon.normal)
        $iNormalPath = $path . '/n_' . $name;
        $image = Engine_Image::factory();
        $image->open($file)
                ->autoRotate()
                ->resize(48, 120)
                ->write($iNormalPath)
                ->destroy();

        // Resize image (icon.square)
        $iSquarePath = $path . '/s_' . $name;
        $image = Engine_Image::factory();
        $image->open($file)
                ->autoRotate();
        $size = min($image->height, $image->width);
        $x = ($image->width - $size) / 2;
        $y = ($image->height - $size) / 2;
        $image->resample($x, $y, $size, $size, 48, 48)
                ->write($iSquarePath)
                ->destroy();

        // Cloud compatibility, put into storage system as temporary files
        $storage = Engine_Api::_()->getItemTable('storage_file');

        // Save/load from session
        if (empty($this->getSession()->tmp_file_id)) {
            // Save
            $iMain = $storage->createTemporaryFile($iMainPath);
            $iProfile = $storage->createTemporaryFile($iProfilePath);
            $iNormal = $storage->createTemporaryFile($iNormalPath);
            $iSquare = $storage->createTemporaryFile($iSquarePath);

            $iMain->bridge($iProfile, 'thumb.profile');
            $iMain->bridge($iNormal, 'thumb.normal');
            $iMain->bridge($iSquare, 'thumb.icon');

            $this->getSession()->tmp_file_id = $iMain->file_id;
        }

        // Save path to session?
//        $_SESSION['TemporaryProfileImg'] = $iMain->map();
//        $_SESSION['TemporaryProfileImgProfile'] = $iProfile->map();
//        $_SESSION['TemporaryProfileImgSquare'] = $iSquare->map();
        // Remove temp files
        @unlink($path . '/p_' . $name);
        @unlink($path . '/m_' . $name);
        @unlink($path . '/n_' . $name);
        @unlink($path . '/s_' . $name);
    }

}
